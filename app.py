from chalice import Chalice
import os
import json
from chalicelib import MESSAGE
# from chalicelib.extract.Pdf2Text import Pdf2Text
# from chalicelib.att_bill_automator
from chalicelib.common.bill_category_enum import bill_category_enum
from chalicelib.extract.Pdf2Text import Pdf2Text
from chalicelib.processor.pre_processor import pre_processor
from chalicelib.miner.bill_at_glance_miner import bill_at_glance_miner
from chalicelib.miner.service_summary_miner import service_summary_miner
from chalicelib.miner.account_details_miner import account_details_miner
from chalicelib.modal.account_details import account_details
from chalicelib.processor.process_individual_phone_numbers import process_individual_phone_numbers
from chalicelib.miner.phone_data_miner import phone_data_miner
from chalicelib.calculate.bill_calculator import bill_calculator
app = Chalice(app_name='pdf_reader')
filename = os.path.join(
    os.path.dirname(__file__), 'chalicelib')
# with open(filename) as f:
    # fil = f

# aws sample end point
@app.route('/')
def index():
    app.log.debug("This is a debug statement")
    return {'hello': 'world'}
# aws end point to access detailed bill
@app.route('/bill/att/{monthyear}/detailed')
def detailed_bill(monthyear):
    print 'detailed bill'
    lst = cal(monthyear, True)
    return json.dumps(lst, default=lambda o: o.__dict__, sort_keys=True, indent=2)

# aws end point to access brief bill
@app.route('/bill/att/{monthyear}')
def hello(monthyear):
    lst = cal(monthyear, False)
    return json.dumps(lst, default=lambda o: o.__dict__, sort_keys=True, indent=2)

def cal(monthyear, should_show_detail_charges):
    base_path = './chalicelib/pdf_verified'
    # chalicelib/passthroughAction-16aug25.pdf
    default_file = 'passthroughAction-Jan17.pdf'
    dir_list = os.listdir(base_path)
    monthyear = monthyear.lower()

    for d in dir_list:
        if '.DS_Store' in d:
           continue
        low = d.lower()
        spl = low.split('-')
        # print "splitttt", spl[1]
        if monthyear in spl[1]:
           default_file = d
    lst = list()
    extractor = Pdf2Text(base_path + '/'+default_file);
    txt = extractor.extractText();
    # print "extracted content------> "
    pre = pre_processor(txt);
    pre.extract_wireless_statement_categroy_raw_txt();
    details = account_details_miner(pre.get__bill_at_glance_raw_data(), pre.get__service_summary_raw_data())
    act_detail = details.get_account_details()
    # print "acct details "
    bill = bill_at_glance_miner(pre.get__bill_at_glance_raw_data())
    # print "balance: ", act_detail.get__current_balance()
    # print "curr balance-->", bill.get_current_balance()
    # print "total due-->", bill.get_total_amount_due()
    # print "current due date-->", bill.get_current_due_date()
    # ssum = service_summary_miner(pre.get__service_summary_raw_data())
    # print "account_charges", ssum.get_account_charges()
    # print "wireless",ssum.get_service_summary_data()
    # act_detail.__bill_cycle_date = bill_at_glance_miner_obj.get_current_due_date()
    # act_detail.__current_balance = bill_at_glance_miner_obj.get_current_balance()
    # act_detail.__previous_balance = bill_at_glance_miner_obj.get_previous_balance()
    # act_detail.__total_balance = bill_at_glance_miner_obj.get_total_amount_due()
    process = process_individual_phone_numbers(txt, act_detail.get__service_summary().get__wirelsss_list())
    # print "_________________________START__________________________________"
    list_raw_data = process.get_individual_phone_data()
    # print list_raw_data
    phone_miner = phone_data_miner(list_raw_data, act_detail.get__service_summary().get__wirelsss_list())
    # phone_miner.mine_phone_data()
    phone_miner.get_monthly_charges()
    cal = bill_calculator(act_detail, should_show_detail_charges)
    res = cal.calculate()
    # print"res",res
    # jsn = json.dumps(res, default=lambda o: o.__dict__, sort_keys=True, indent=2)
    # lst.append(res)
    return res

def main():
    print "------------------------main method-------------------------"
    lst = cal('Jan2017', False)
    print json.dumps(lst, default=lambda o: o.__dict__, sort_keys=True, indent=2)
    # return jsn
    # print "--------------------------END-----------------------------------"
if __name__== "__main__":
    # print "hello world"
    main()
