# Att bill calculator

- make sure to install python 2.7(Python 2.7 pre-installed by Apple)
- run the follwoing command in terminal or any unix shell to update pip ```sudo -H pip install -U pip```
- run the follwing command in terminal or any unix shell to install the dependency packages for this project ```sudo pip install chalice pyPdf enum34 jsonschema```
- steps to download the att bill:
    * click on any bill pdf(which opens in the pdf in new window as html page)
    * instead of downloading the pdf, click on the print icon which will be at the top right corner and save as pdf
    * once downlaoded move the pdf to the follwing location ```project_root_path/chalicelib/pdf_verified/```
- run the follwing command in terminal or any unix shell to execute the project(on the project_root_path, since app.py is available on the root) ``` python app.py```
- verify the output in the in terminal or unix shell where ever you run the above command
