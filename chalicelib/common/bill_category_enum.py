
from enum import Enum

class bill_category_enum(Enum):

    BILL_CYCLE_DATE = "Page:"
    BILL_CYCLE_DATE_END = "Bill Cycle Date:"
    SERVICE_SUMMARY= "Service Summary"
    SERVICE_SUMMARY_END = "Manage Your Account"
    BILL_AT_GLANCE = "Bill-At-A-Glance"
    BILL_AT_GLANCE_END = "Service Summary"
