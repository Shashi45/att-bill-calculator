import pyPdf
from chalicelib.db.rawDataToDB import rawDataToDB

class Pdf2Text:

    __file_name = ''
    def __init__(self, name):
        self.__file_name = name


    def extractText(self):
        print "extracting data for the file name...",self.__file_name
        # base_path = '/Users/dharaniprasadenamanagandla/workspace/pdf_reader/pdfs/' + self.__file_name
        p = file(self.__file_name, "rb")
        pdf = pyPdf.PdfFileReader(p)
        content = ""
        for i in range(0, 8):
            page = pdf.getPage(i)
            str = " "
            # print page
            for line in page.extractText().splitlines(True):
                str += line + "\n"+ "\n"
            content += str;
        # print "end content"
        encodedExtractedString = content.encode("ascii", "ignore");
        encodedExtractedString = encodedExtractedString.replace("\n", "");

        # -----------mongodb to save raw data start
        # rdb = rawDataToDB()
        # date = (self.__file_name.split('-')[1]).split(".")[0]
        # extracted_json = {
        # 'date': date,
        # 'raw_data': encodedExtractedString,
        # 'pdf_name': self.__file_name
        # }
        # # rdb.save_raw_data(extracted_json)
        # find_criteria = {'date': date}
        # rdb.find_raw_data(find_criteria)
        # -----------mongodb to save raw data end
        with open("./chalicelib/extract/extractedContent.txt", 'a') as text_file:
            text_file.write(encodedExtractedString)
        # print "end writing to file"
        return encodedExtractedString




# extractor = Pdf2Text();
# extractor.extractText();
