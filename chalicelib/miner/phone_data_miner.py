
import re
from chalicelib.modal.monthly_charges import monthly_charges
from chalicelib.modal.voice_usage_summary import voice_usage_summary
from chalicelib.modal.item import item
from chalicelib.modal.wireless_equip import wireless_equip
from chalicelib.modal.other_credit_charges import other_credit_charges
from chalicelib.modal.surcharges import surcharges
from chalicelib.modal.govt_fees_and_taxes_charges import govt_fees_and_taxes_charges
from chalicelib.modal.account_activity import account_activity
from chalicelib.modal.one_time_charges import one_time_charges
from chalicelib.modal.data_usage_summary import data_usage_summary
from chalicelib.modal.other_purchases_3rd_party import other_purchases_3rd_party

class phone_data_miner:

    __dict_phone_data_raw = {}
    __wireless_list = []
    __monthly_charges_start_regex = r'Monthly Charges-\w*\s{1}\d{2}thru\w*\s{1}\d{2}'
    __montly_charges_end_regex = r'Total Monthly Charges\d*\.\d{2}'
    __regex_monthly_item_start = r'^\d{1,2}\.'
    __regex_montly_item_end = r'\d*\.\d{2}(CR){0,1}'
    __o_c_and_c_charges_start_header = 'Other Charges and Credits'
    __o_c_and_c_charges_end_header = 'Total Other Charges & Credits'
    __regex_data_usage_start_header = r'Data Usage Summary'
    __regex_data_usage_end_header = r'1 Gigabyte \(GB\) = 1024MB, 1 Megabyte \(MB\) = 1024KB'
    __regex_voice_summary_header = r'Voice Usage Summary'
    __regex_account_activity_start_header = r'Account Activity'
    __regex_account_activity_end_header = r'Total Account Activity'
    __regex_wireless_equip_start = r'Wireless Equipment Charges'
    __regex_wireless_equip_end = r'Balance Remaining after Current Installment:\$\d*,{0,1}\d*\.\d{2}'
    __regex_one_time_charges_header = r'One-Time ChargesDateDescription'
    __regex_surcharges_and_other_start = r'Surcharges and Other Fees'
    __regex_surcharges_and_other_end = r'Total Surcharges and Other Fees\d*\.\d{2}'
    __regex_govt_fees_and_taxes_start = r'Government Fees and Taxes'
    __regex_govt_fees_and_taxes_end = r'Total Government Fees and Taxes\d*\.\d{2}'
    __regex_wl_install_plan_est_dt = r'-\sEst\.\son\s\d{2}/\d{2}/\d{2}'
    __regex_wl_install_plan_id = r'Installment Plan ID:'
    __regex_wl_amount_financed = r'Amount Financed:'
    __regex_wl_date_description = r'DateDescription'


    def __init__(self, dict_phone_data_raw, wireless_list):
        self.__dict_phone_data_raw = dict_phone_data_raw
        self.__wireless_list = wireless_list
        # print self.__list_phone_data_raw

    def mine_phone_data(self):
        for phone_data in self.__dict_phone_data_raw:
            print
            # print phone_data
            # Monthly Charges



    def get_monthly_charges(self):

        for wless in self.__wireless_list:
            # print "_____________monthly start___________"
            wless.set__monthly_charges(self.get_monthly_data(wless.get__name()))
            # print "_____________monthly---------------"
            wless.set__other_credit_and_tax_charges(self.get_other_charges_data(wless.get__name()))
            # print "wireless-------->>>>>>>>>>>", wless



    def get_monthly_data(self, key):
        # print "rawww--------",self.__dict_phone_data_raw
        match_start = re.search(self.__monthly_charges_start_regex, self.__dict_phone_data_raw[key])
        # start_index = self.__dict_phone_data_raw[key].find(self.__monthly_charges_start_header)
        match = re.search(self.__montly_charges_end_regex,  self.__dict_phone_data_raw[key])

        monthly_data = (self.__dict_phone_data_raw[key])[match_start.end() : match.end()]
        # print "monthly   --------      ",monthly_data
        monthly_split = monthly_data.split("    ")
        # print "monthly split ", monthly_data
        monthly_charges_obj  = monthly_charges()
        monthly_charges_obj.set__item_list(self.get_items_list_from_raw_data(monthly_split))
        self.__dict_phone_data_raw[key] = (self.__dict_phone_data_raw[key])[match.end() : ]
        # print "monthlyyyyyyyyyyyyyyyy",monthly_charges_obj
        return monthly_charges_obj

    def get_items_list_from_raw_data(self, split_data):
        items_list = list()
        for data in split_data:
            data = data.strip()

            continue_index = data.find('- Continued')
            if  (not re.search(self.__regex_monthly_item_start, data)) and continue_index != -1:
                data = (data[continue_index + len('- Continued') : ]).strip()
            while(True):
                if data != '':
                   itm = item()
                   s_no_match = re.search(self.__regex_monthly_item_start, data)
                   if s_no_match:
                      # print "itemmmmm",data[s_no_match.start() : s_no_match.end()-1]
                      itm.set__serial_number(data[s_no_match.start() : s_no_match.end()-1])
                   else:
                       break;
                   charge_match = re.search(self.__regex_montly_item_end, data)
                   if charge_match:
                      # print "charge ", data[charge_match.start() : charge_match.end()-1]
                      price = data[charge_match.start() : charge_match.end()]
                      if "CR" in price:
                          itm.set__is_item_credit("yes")
                          price = price[ : len(price)-2]
                      itm.set__price(price)

                   if s_no_match and charge_match:
                      itm.set__type(data[s_no_match.end() : charge_match.start()])
                      # print "jsonn>>>>>>>>>>>>>>>>>>>>>>>",itm.toJSON()
                   # items_list.append(itm)
                   if charge_match and charge_match.end() != len(data):
                      data = data[charge_match.end() : ]
                      items_list.append(itm)
                      # print itm
                      # items_list.append(itm)
                      continue;
                   else:
                       # print itm
                       items_list.append(itm)
                       break;
                else:
                   break;
        # print "list^^^^^^^^", items_list
        items_len = len(items_list)
        if(items_len > 0):
            items_list[0].set__start_of_group("start")
            items_list[items_len - 1].set__end_of_group('end')
        return items_list
               # else:
               #    itm = None
               # if itm != None:
               #    print itm

    def get_other_charges_data(self, key):
        occ = other_credit_charges()
        start_match = re.search(self.__o_c_and_c_charges_start_header,self.__dict_phone_data_raw[key])
        end_match =  re.search(self.__o_c_and_c_charges_end_header,self.__dict_phone_data_raw[key])
        # print "---->>>>>>", (self.__dict_phone_data_raw)
        occ.set__data_usage(self.get_data_usage_charges_data(self.__dict_phone_data_raw[key]))
        occ.set__account_activity(self.get_account_activity_data(self.__dict_phone_data_raw[key]))
        occ.set__voice_usage_summary(self.get_voice_summary_data(self.__dict_phone_data_raw[key]))
        occ.set__wireless_equip_charges(self.get_wireless_equip_charges_data((self.__dict_phone_data_raw[key])[start_match.end() : ]))
        occ.set__one_time_charges(self.get_one_time_charges_data(self.__dict_phone_data_raw[key]))
        occ.set__surcharges(self.get_surcharges_and_other_data(self.__dict_phone_data_raw[key]))
        govt_obj = self.get_govt_fees_and_taxes_data(self.__dict_phone_data_raw[key])
        occ.set__govt_fees_and_taxes(govt_obj)
        occ.set__other_purchases_3rd_party(self.get_other_purchases_3rd_party_data(self.__dict_phone_data_raw[key], govt_obj))
        # print "$$$$$$$$$$", occ
        return occ

    def get_data_usage_charges_data(self, raw_data):

        start_match = re.search(self.__regex_data_usage_start_header, raw_data)
        end_match = re.search(self.__regex_data_usage_end_header, raw_data)
        if start_match and end_match:
           data_usage_data = raw_data[start_match.end() : end_match.start()]

           regex_total_data = r'Total GB Used\s*\d*\.\d{2}'
           total_match = re.search(regex_total_data, data_usage_data)
           data = data_usage_data[total_match.end() : ]
           s_no_match = re.search(self.__regex_monthly_item_start, data)
           itm = item()
           if s_no_match:
              # print "itemmmmm",data[s_no_match.start() : s_no_match.end()-1]
              itm.set__serial_number(data[s_no_match.start() : s_no_match.end()-1])
              data = data[s_no_match.end() : ]
           price_regex = r'(\d*\.\d{2}(CR){0,1}\s*)$'
           filter_data_regex = r'\s{1,10}\d*\.\d{2}'
           filter_data_match = re.search(filter_data_regex, data)
           if filter_data_match:
              data = data[filter_data_match.end() : ]
           # data = data[]
           charge_match = re.search(price_regex, data)
           # print "prieceeeeeeeee", data, '---'
           if charge_match:
              # print "charge ", data[charge_match.start() : charge_match.end()-1]
              price = data[charge_match.start() : charge_match.end()]
              if "CR" in price:
                  itm.set__is_item_credit("yes")
                  price = price[ : len(price)-2]
              itm.set__price(price)
           itm.set__type('data used')
           dusage = data_usage_summary()
           dusage.set__item(itm)
           # print "usageeeeeeeeeeeeeeeeeee", dusage
           return dusage



    def get_voice_summary_data(self, raw_data):
        print
        voice_match_start = re.search(self.__regex_voice_summary_header, raw_data)
        end_match = re.search(self.__regex_wireless_equip_start, raw_data)
        if voice_match_start:
           if not end_match:
              end_match = re.search(self.__regex_surcharges_and_other_start, raw_data)
           voice_data = raw_data[voice_match_start.end() : end_match.start()]
           # print "@@@@@@@@@@@@@@@@@", voice_data
           internationl_voice_header = "International Long Distance"
           index = voice_data.find(internationl_voice_header)
           item_data_partial = voice_data[index + len(internationl_voice_header) : ]
           item_data_partial_split = (voice_data[index + len(internationl_voice_header) : ]).split("    ")
           item = self.get_items_list_from_raw_data(item_data_partial_split)

           if len(item) > 0:

              voice_usage_summary__obj = voice_usage_summary()

              voice_usage_summary__obj.set__item(item[0])
              voice_usage_summary__obj.set__mins(item_data_partial_split[1])

              return voice_usage_summary__obj

    def get_account_activity_data(self, raw_data):

        aa_match_start = re.search(self.__regex_account_activity_start_header, raw_data)
        aa_match_end = re.search(self.__regex_account_activity_end_header, raw_data)
        if aa_match_start and aa_match_end:
            aa_data = raw_data[aa_match_start.end() : aa_match_end.start()]
            # print aa_data
            split_data = aa_data.split("                       ")
            aa = account_activity()
            aa.set__items(self.get_items_list_from_raw_data(split_data))
            return aa

    def get_one_time_charges_data(self, raw_data):
        onetime_match = re.search(self.__regex_one_time_charges_header, raw_data)
        if onetime_match:
           aa_data = raw_data[onetime_match.end() : ]
           data =aa_data.split("    ")[0]
           # print "datadatadatadatadatadatadatadata", data
           itm = item()
           s_no_match = re.search(self.__regex_monthly_item_start, data)
           if s_no_match:
              # print "itemmmmm",data[s_no_match.start() : s_no_match.end()-1]
              itm.set__serial_number(data[s_no_match.start() : s_no_match.end()-1])
              data = data[s_no_match.end() : ]
           charge_match = re.search(self.__regex_montly_item_end, data)
           if charge_match:
              # print "charge ", data[charge_match.start() : charge_match.end()-1]
              price = data[charge_match.start() : charge_match.end()]
              if "CR" in price:
                  itm.set__is_item_credit("yes")
                  price = price[ : len(price)-2]
              itm.set__price(price)
           if s_no_match and charge_match:
              itm.set__type(data[s_no_match.end()-2 : charge_match.start()])
           otime = one_time_charges()
           otime.set__item(itm)
           # print "oooooooooooooooo", otime
           return otime

    def get_wireless_equip_charges_data(self, raw_data):
        start_match = re.search(self.__regex_wireless_equip_start, raw_data)
        end_match = re.search(self.__regex_wireless_equip_end, raw_data)
        if end_match:
           wirelss_equip_data = raw_data[start_match.end() : end_match.end()]

           plan_id_match = re.search(self.__regex_wl_install_plan_id, wirelss_equip_data)
           plan_dt_match = re.search(self.__regex_wl_install_plan_est_dt, wirelss_equip_data)
           amount_financed_match = re.search(self.__regex_wl_amount_financed, wirelss_equip_data)
           dt_financed_desc_match = re.search(self.__regex_wl_date_description, wirelss_equip_data)
           equip = wireless_equip()
           equip.set__plan_id(wirelss_equip_data[plan_id_match.end() : plan_dt_match.start()])
           equip.set__phone_modal(wirelss_equip_data[plan_dt_match.end() : amount_financed_match.start()])
           equip.set__device_price(wirelss_equip_data[amount_financed_match.end() : dt_financed_desc_match.start()])
           equip.set__balance_remaining('')
           # print wirelss_equip_data[plan_id_match.end() : plan_dt_match.start()]
           # print wirelss_equip_data[plan_dt_match.end() : amount_financed_match.start()]
           # print wirelss_equip_data[amount_financed_match.end() : dt_financed_desc_match.start()]
           item_data = wirelss_equip_data[dt_financed_desc_match.end() : ]
           # print item_data
           regex_itm_start = r'\d{2}/\d{2}Installment\s{1,2}\d{1,2}\sof\s\d{1,2}'
           regex_balance_remaining = r'Balance Remaining after Current Installment:\d*\.\d{2}'
           itm_match = re.search(regex_itm_start, item_data)
           # print"wireless equip charges********> ", item_data
           if itm_match:
              # print item_data[itm_match.end() : ]
              data = item_data[itm_match.end() : ]

              s_no_match = re.search(self.__regex_monthly_item_start, item_data)
              itm_split = item_data[itm_match.end():].split('    ')
              itm = item()
              itm.set__serial_number(item_data[s_no_match.start() : s_no_match.end() - 1])
              # TODO use this desc for installment info
              itm.set__type(item_data[itm_match.start() : itm_match.end()])
              price = itm_split[0]
              itm.set__price(price)
              if "CR" in price:
                  itm.set__is_item_credit("yes")
              equip.set__item(itm)
           # print equip
           raw_data = raw_data[end_match.end() : ]
           # print raw_data
           return equip;

    def get_surcharges_and_other_data(self, raw_data):
        start_match = re.search(self.__regex_surcharges_and_other_start, raw_data)
        end_match = re.search(self.__regex_surcharges_and_other_end, raw_data)
        # print "surcharges and other fees ~~~~~~~~~~", raw_data[start_match.end() : end_match.end()]
        surcharges_data = raw_data[start_match.end() : end_match.end()]
        split_data = surcharges_data.split("    ")
        # print "%%%%%%%%%%%%%%%%%", split_data
        surcharges_obj = surcharges()
        surcharges_obj.set__items(self.get_items_list_from_raw_data(split_data))
        raw_data = raw_data[end_match.end() : ]
        # print "````````````````", surcharges_obj
        return surcharges_obj

    def get_govt_fees_and_taxes_data(self, raw_data):
        start_match = re.search(self.__regex_govt_fees_and_taxes_start, raw_data)
        end_match = re.search(self.__regex_govt_fees_and_taxes_end, raw_data)
        # print "govt and fees ````````````>>>>>>", raw_data[start_match.end() : end_match.end()]
        govt_data = raw_data[start_match.end() : end_match.end()]
        split_data = govt_data.split("    ")
        # print "%%%%%%%%%%%%%%%%%", split_data
        govt_fees_and_taxes_charges_obj = govt_fees_and_taxes_charges()
        # if self.
        govt_fees_and_taxes_charges_obj.set__items(self.get_items_list_from_raw_data(split_data))
        raw_data = raw_data[end_match.end() : ]
        return govt_fees_and_taxes_charges_obj
    __regex_3rd_party_start = r'3rd Party Subscriptions & Purchases'
    __regex_3rd_party_end = r'Total 3rd Party Subscriptions & Purchases'

    def get_other_purchases_3rd_party_data(self, raw_data, govt_taxes_obj):

        start_match = re.search(self.__regex_3rd_party_start, raw_data)
        end_match = re.search(self.__regex_3rd_party_end, raw_data)
        if start_match and end_match:
           other_purchases = raw_data[start_match.end() : end_match.start()]

           # purchases_3rd
           govt_match = re.search(r'Total 3rd Party Purchases', other_purchases)
           if govt_match:
              govt_raw_data = other_purchases[govt_match.start() : ]
              purchases_3rd = other_purchases[ : govt_match.start()]

              purchases_3rd = purchases_3rd.replace('3rd Party PurchasesTo manage your purchases or add Purchase Blocker (a freeservice that blocks mobile subscriptions and purchases) goto att.com/directbill or call 800-331-0500 from any phone.DateDescription', '').strip()
              purchases_3rd = purchases_3rd.replace('Merchant:TextCollect.comContact:866-229-6829', '').strip()
              match_date = re.search(r'\d{2}/\d{2}', purchases_3rd)
              purchases_3rd  = purchases_3rd.replace(purchases_3rd[match_date.start() : match_date.end()], '')
              # print "--------", purchases_3rd
              # purchases_3rd = purchases_3rd.replace(r"\d{2}/\d{2}", " ")
              # print "matchhhhhhhhhhhhhh", purchases_3rd[re.search(r'\d{2}/\d{2}', purchases_3rd).start():re.search(r'\d{2}/\d{2}', purchases_3rd).end()]
              split = purchases_3rd.split("    ")
              # print "splittttttttt", split
              # print "splittttttttt", self.get_items_list_from_raw_data(split)
              # for i in self.get_items_list_from_raw_data(split):
              #     print i
              other_purchases_obj = other_purchases_3rd_party()
              other_purchases_obj.set__items(self.get_items_list_from_raw_data(split))
              govt = self.get_govt_fees_and_taxes_data(govt_raw_data)
              # print "---------raw_data------", other_purchases
              if govt_taxes_obj:
                 govt_taxes_obj.get__items().extend(govt.get__items())
                 # print "##############",govt_taxes_obj
              return other_purchases_obj


    def get_other_charges_and_credits(self):
        print
        # Other Charges and Credits
        # - Wireless Equipment Charges
        # Other Charges and Credits - Continued
        # - Surcharges and Other Fees
        # - Government Fees and Taxes
        # end Total Other Charges & Credits
        # end Total for 248 986-5123


        # print "phoneeeeeeeeeeeee",(self.__dict_phone_data_raw[key])[start_index : ]
