
from chalicelib.modal.account_details import account_details
from chalicelib.miner.bill_at_glance_miner import bill_at_glance_miner
from chalicelib.miner.service_summary_miner import service_summary_miner

class account_details_miner:

    # __bill_cycle_date
    # __current_balance
    # __previous_balance
    # __total_balance
    # __new_charges_due
    __bill_at_glance_raw = ''
    __service_summary_raw = ''

    def __init__(self, bill_at_glance_raw, service_summary_raw):

        self.__bill_at_glance_raw = bill_at_glance_raw
        self.__service_summary_raw = service_summary_raw

    def get_account_details(self):
        act_detail = account_details()
        service_summary_miner_obj = service_summary_miner(self.__service_summary_raw)
        act_detail.set__service_summary(service_summary_miner_obj.get_service_summary_data())
        bill_at_glance_miner_obj = bill_at_glance_miner(self.__bill_at_glance_raw)

        # act_detail.set__service_summary(service_summary_miner_obj.get_service_summary_data())
        act_detail.set__previous_balance(bill_at_glance_miner_obj.get_previous_balance())
        current_balance = bill_at_glance_miner_obj.get_current_balance()
        act_detail.set__current_balance(current_balance)
        act_detail.set__bill_cycle_date(bill_at_glance_miner_obj.get_current_due_date())
        # print "bill ---------",act_detail.get__current_balance()
        act_detail.set__total_balance(bill_at_glance_miner_obj.get_total_amount_due())

        # act_detail.__current_balance = bill_at_glance_miner_obj.get_current_balance()
        act_detail.__new_charges_due = bill_at_glance_miner_obj.get_current_due_date()
        # print "&&&&", act_detail
        return act_detail
