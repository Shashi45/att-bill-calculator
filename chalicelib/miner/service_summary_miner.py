
import re
from chalicelib.modal.wireless import wireless
from chalicelib.modal.service_summary import service_summary

class service_summary_miner:

    __account_charges_header = 'Account Charges'
    __service_summary_raw_data=''
    __regex_decimal_with_precision_2 = r'^(\$\d*\.\d{2})'
    __regex_find_number_charges_and_page = r'^(\$\d*\.\d+\s{1}\d*-\d*\s{2}\d{1})'

    def __init__(self, raw_data):
        self.__service_summary_raw_data = raw_data


    def get_account_charges(self):
        # print ""
        start_index = self.__service_summary_raw_data.find(self.__account_charges_header)
        end_index = self.__service_summary_raw_data.find("  ")
        account_charges = self.__service_summary_raw_data[start_index : end_index].strip()
        self.__truncate_raw_string(end_index)
        return account_charges

    def __truncate_raw_string(self, start_index):
        # print ""
        self.__service_summary_raw_data = self.__service_summary_raw_data[start_index : ].strip()
        # print "truncated...", self.__service_summary_raw_data

    def get_service_summary_data(self):
        self.get_account_charges()
        service_sumry = service_summary()
        service_sumry.set__wirelsss_list(self.get_wireless_data())

        return service_sumry
        # service_summary.__total_charges =

    def get_wireless_data(self):
        # print "  ----  ",    self.__service_summary_raw_data.split("    ")[1]
        wireless_raw = self.__service_summary_raw_data.split("    ")[1]
        # print "___________ ", wireless_raw
        self.__service_summary_raw_data = wireless_raw
        wireless_list = list()
        while(True):
            match = re.search(self.__regex_find_number_charges_and_page, self.__service_summary_raw_data)
            if match:
               start = match.start()
               end = match.end()
               wireless_string = self.__service_summary_raw_data[start : end]
               charges = re.search(self.__regex_decimal_with_precision_2, wireless_string)
               wirele = wireless()
               wirele.set__individual_charges(wireless_string[charges.start() : charges.end()])
               split_str = wireless_string.split("  ")
               wirele.set__phone_number((split_str[0])[charges.end():])
               wirele.set__page_num_on_pdf(split_str[1])
               wireless_list.append(wirele)
               # print "phone ", wirele.__phone_number
               # print "charges ", wirele.__individual_charges
               # print "page ", wirele.__page_num_on_pdf
               self.__truncate_raw_string(end)
            else :
              break
        # print len(wireless_list)
        return wireless_list
