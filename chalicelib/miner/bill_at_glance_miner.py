
from chalicelib.common.bill_category_enum import bill_category_enum
import re

class bill_at_glance_miner:
    __previous_balance_header = 'Previous Balance'
    __current_charges_header = 'New Charges'
    __total_amt_due_header = ' Total Amount Due'
    __current_charges_due_header = 'New Charges Due in Full by'
    __length_of_raw_data=0;

    def __init__(self, raw_data):
        # print "...raw data ...", raw_data
        self.__bill_at_glance_raw_data = raw_data
        self.__length_of_raw_data = len(raw_data)

    def get_previous_balance(self):

        start_index = self.__bill_at_glance_raw_data.find(self.__previous_balance_header);
        balance = (self.__bill_at_glance_raw_data[len(bill_category_enum.BILL_AT_GLANCE.value)
            : start_index]).strip()
        # truncating the previouse balance from raw text_file
        # self.__bill_at_glance_raw_data = self.__bill_at_glance_raw_data[start_index + len(self.__previous_balance_header):]
        self.__truncate_raw_string(start_index + len(self.__previous_balance_header))

        return balance;

    def get_current_balance(self):
        curr_header_len = len(self.__current_charges_header)
        start_index = self.__bill_at_glance_raw_data.find(self.__current_charges_header)
        sub_string = self.__bill_at_glance_raw_data[(start_index + curr_header_len) : (start_index + curr_header_len + 10)]
        curr_balance = sub_string.split("  ")[0].strip()
        self.__truncate_raw_string(start_index + len(self.__current_charges_header)+ len(curr_balance))
        return sub_string.split("  ")[0].strip()


    def get_total_amount_due(self):
        # print ""
        total_amount_due = self.__bill_at_glance_raw_data.split("  ")[0].strip()
        start_index = self.__bill_at_glance_raw_data.find(self.__total_amt_due_header)
        self.__truncate_raw_string(start_index+len(self.__total_amt_due_header))
        return total_amount_due

    def get_current_due_date(self):

        match = re.search(self.__current_charges_due_header, self.__bill_at_glance_raw_data)
        if match:
           current_due_date = self.__bill_at_glance_raw_data[match.end() : ]
           self.__truncate_raw_string(len(self.__bill_at_glance_raw_data))
           return current_due_date

    def __truncate_raw_string(self, start_index):
        # print ""
        self.__bill_at_glance_raw_data = self.__bill_at_glance_raw_data[start_index : ].strip()
        # print "truncated...", self.__bill_at_glance_raw_data





# bill = bill_at_glance_miner("Bill-At-A-Glance$600.39  Previous BalancePayment - 08/03                                   "+
# "$300.27CRAdjustments$0.00  Past Due - Please Pay Immediately$300.12  New Charges$278.56  $578.68  Total Amount DueNew Charges Due in Full bySep 19, 2017")
# print "balance is: ", bill.get_previous_balance();
