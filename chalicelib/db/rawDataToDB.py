
from pymongo import MongoClient

class rawDataToDB:



    def save_raw_data(self, raw_data_doc):

        client = MongoClient()
        db = client.test
        posts = db.posts
        post_data = {
        'hello': 'world'
        }
        result = posts.insert_one(raw_data_doc)
        print('One post: {0}'.format(result.inserted_id))

    def find_raw_data(self, find_criteria):
        client = MongoClient()
        db = client.test
        posts = db.posts
        print posts.find_one(find_criteria)
