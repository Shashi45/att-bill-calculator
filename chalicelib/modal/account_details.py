

class account_details:

    __bill_cycle_date=''
    __current_balance=''
    __previous_balance=''
    __total_balance=''
    __new_charges_due=''
    __service_summary=''



    def get__current_balance(self):
        return self.__current_balance
    def set__current_balance(self, curr_balance):
        self.__current_balance = curr_balance

    def get__bill_cycle_date(self):
        return self.__bill_cycle_date
    def set__bill_cycle_date(self, bill_cycle_date):
        self.__bill_cycle_date = bill_cycle_date

    def get__previous_balance(self):
        return self.__previous_balance
    def set__previous_balance(self, previous_bal):
        self.__previous_balance = previous_bal

    def get__total_balance(self):
        return self.__total_balance
    def set__total_balance(self, total):
        self.__total_balance = total
    def get__new_charges_due(self):
        return self.__new_charges_due
    def set__new_charges_due(self, new_due):
        self.__new_charges_due = new_due
    def get__service_summary(self):
        return self.__service_summary
    def set__service_summary(self,__service_summary):
        self.__service_summary = __service_summary
    # def get
    # def set

    def __str__(self):
        return "billCycleDate: {}, currentBalance: {}, previousBalane: {}, totalBalance: {}, newChargesDue: {}, serviceSummary: {}".format(self.__bill_cycle_date, self.__current_balance, self.__previous_balance, self.__total_balance,self.__new_charges_due, self.__service_summary)
