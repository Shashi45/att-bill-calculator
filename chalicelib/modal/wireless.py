

from json import JSONEncoder
class wireless(JSONEncoder):

    __phone_number=''
    __individual_charges=''
    __page_num_on_pdf=''
    __name=''
    __monthly_charges = ''
    __other_credit_and_tax_charges = ''
    __common_items = []
    __specific_items = []

    def get__phone_number(self):
        return self.__phone_number
    def set__phone_number(self, number):
        self.__phone_number = number
    def get__individual_charges(self):
        return self.__individual_charges
    def set__individual_charges(self, ind_charges):
        self.__individual_charges = ind_charges
    def get__page_num_on_pdf(self):
        return self.__page_num_on_pdf
    def set__page_num_on_pdf(self, page):
        self.__page_num_on_pdf = page
    def get__name(self):
        return self.__name
    def set__name(self, name):
        self.__name = name
    def get__monthly_charges(self):
        return self.__monthly_charges
    def set__monthly_charges(self, monthly):
        self.__monthly_charges = monthly
    def get__other_credit_and_tax_charges(self):
        return self.__other_credit_and_tax_charges
    def set__other_credit_and_tax_charges(self, occ):
        self.__other_credit_and_tax_charges = occ
    def get__common_items(self):
        return self.__common_items
    def set__common_items(self, common):
        self.__common_items = common
    def get__specific_items(self):
        return self.__specific_items
    def set__specific_items(self, spec):
        self.__specific_items = spec
    def __str__(self):
        return "phone: {}, indCharges: {}, pageNum: {}, Name: {}, MonthlyCharges: {}, OCCCharges: {}".format(self.__phone_number, self.__individual_charges, self.__page_num_on_pdf, self.__name, self.__monthly_charges, self.__other_credit_and_tax_charges)
