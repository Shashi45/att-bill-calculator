


class other_credit_charges:

    __data_usage = ''
    __surcharges = '';
    __account_activity = '';
    __voice_usage_summary = '';
    __wireless_equip_charges = '';
    __one_time_charges = '';
    __govt_fees_and_taxes = '';
    __other_purchases_3rd_party = ''
    __total_o_c_c = '';

    def get__data_usage(self):
        return self.__data_usage
    def set__data_usage(self, data):
        self.__data_usage = data
    def get__surcharges(self):
        return self.__surcharges
    def set__surcharges(self, sur):
        self.__surcharges = sur
    def get__account_activity(self):
        return self.__account_activity
    def set__account_activity(self, aa):
        self.__account_activity = aa
    def get__voice_usage_summary(self):
        return self.__voice_usage_summary
    def set__voice_usage_summary(self, voice):
        self.__voice_usage_summary = voice
    def get__wireless_equip_charges(self):
        return self.__wireless_equip_charges
    def set__wireless_equip_charges(self, equip):
        self.__wireless_equip_charges = equip
    def get__one_time_charges(self):
        return self.__one_time_charges
    def set__one_time_charges(self, otime):
        self.__one_time_charges = otime
    def get__govt_fees_and_taxes(self):
        return self.__govt_fees_and_taxes
    def set__govt_fees_and_taxes(self, govt):
        self.__govt_fees_and_taxes = govt
    def get__other_purchases_3rd_party(self):
        return self.__other_purchases_3rd_party
    def set__other_purchases_3rd_party(self, data):
        self.__other_purchases_3rd_party = data
    def get__total_o_c_c(self):
        return self.__total_o_c_c
    def set__total_o_c_c(self, total):
        self.__total_o_c_c = total
    def __str__(self):
        return "dataUsage: {}, surcharge: {}, accountActivity: {}, voiceUsageSummary: {}, wirelessEquip: {}, oneTime: {}, govtFeesAndTaxes: {}, 3rdParty: {}, total: {}".format(self.__data_usage, self.get__surcharges(), self.__account_activity, self.__voice_usage_summary, self.__wireless_equip_charges, self.__one_time_charges, self.__govt_fees_and_taxes, self.__other_purchases_3rd_party, self.__total_o_c_c);
