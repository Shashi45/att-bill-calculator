


class individual_bill_response:

    wireless_number = '1'
    common_charges = '1'
    specific_charges_list = []
    specific_charges = '1'
    total_charges = ''

    def getwireless_number(self):
        return self.wireless_number
    def setwireless_number(self, n):
        self.wireless_number = n

    def getcommon_charges(self):
        return self.common_charges
    def setcommon_charges(self, n):
        self.common_charges = n
    def getspecific_charges_list(self):
        return specific_charges_list
    def setspecific_charges_list(self, specific):
        self.specific_charges_list = specific
    def getspecific_charges(self):
        return self.specific_charges
    def setspecific_charges(self, n):
        self.specific_charges = n
    def gettotal_charges(self):
        return self.total_charges
    def settotal_charges(self, n):
        self.total_charges = n
    def __str__(self):
        return "wirelessNumber: {}, commonCharges: {}, specificCharges: {}, totalCharges: {}".format(self.wireless_number, self.common_charges, self.specific_charges, self.total_charges)
