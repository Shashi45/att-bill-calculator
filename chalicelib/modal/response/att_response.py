




class att_response:

    wireless_number = '1'
    common_charges_list = []
    individual_bill_details = ''
    common_charges = '1'
    specific_charges_list = []
    specific_charges = '1'
    total_charges = ''
    bill_cycle_due_by = ''

    def getwireless_number(self):
        return self.wireless_number
    def setwireless_number(self, n):
        self.wireless_number = n
    def getcommon_charges_list(self):
        return self.common_charges_list
    def setcommon_charges_list(self, common):
        self.common_charges_list = common
    def getindividual_bill_details(self):
        return self.individual_bill_details
    def setindividual_bill_details(self, bill):
        self.individual_bill_details = bill
    def getcommon_charges(self):
        return self.common_charges
    def setcommon_charges(self, n):
        self.common_charges = n
    def getspecific_charges_list(self):
        return specific_charges_list
    def setspecific_charges_list(self, specific):
        self.specific_charges_list = specific
    def getspecific_charges(self):
        return self.specific_charges
    def setspecific_charges(self, n):
        self.specific_charges = n
    def gettotal_charges(self):
        return self.total_charges
    def settotal_charges(self, n):
        self.total_charges = n
    def getbill_cycle(self):
        return self.bill_cycle_due_by
    def setbill_cycle(self, n):
        self.bill_cycle_due_by = n
    def __str__(self):
        return "wirelessNumber: {}, commonCharges: {}, specificCharges: {}, totalCharges: {}, billCycle: {}".format(self.wireless_number, self.common_charges, self.specific_charges, self.total_charges, self.bill_cycle)
