

import json
class item:


    __serial_number = ''
    __type = ''
    __price = ''
    __is_item_credit = ''
    __start_of_group = ''
    __end_of_group = ''
    __wireless_number = ''


    def get__serial_number(self):
        return self.__serial_number
    def set__serial_number(self, num):
        self.__serial_number = num
    def get__type(self):
        return self.__type
    def set__type(self, type):
        self.__type = type
    def get__price(self):
        return self.__price
    def set__price(self, price):
        self.__price = price
    def get__is_item_credit(self):
        return self.__is_item_credit
    def set__is_item_credit(self, credit):
        self.__is_item_credit = credit
    def get__start_of_group(self):
        return self.__start_of_group
    def set__start_of_group(self, startGroup):
        self.__start_of_group = startGroup
    def get__end_of_group(self):
        return self.__end_of_group
    def set__end_of_group(self, endGroup):
        self.__end_of_group = endGroup
    def get__wireless_number(self):
        return self.__wireless_number
    def set__wireless_number(self, num):
        self.__wireless_number = num
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
    def __str__(self):
        return "serialNum: {}, type: {}, price: {}, isCredit: {}, startGroup: {}, endGroup: {}".format(self.__serial_number, self.__type, self.__price, self.__is_item_credit, self.__start_of_group, self.__end_of_group)
