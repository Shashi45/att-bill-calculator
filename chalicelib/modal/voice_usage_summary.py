


class voice_usage_summary:

    __item=''
    __mins = ''


    def get__item(self):
        return self.__item
    def set__item(self, itm):
        self.__item = itm
    def get__mins(self):
        return self.__mins
    def set__mins(self, min):
        self.__mins = min

    def __str__(self):
        return "item: {}, mins: {}".format(self.__item, self.__mins)
