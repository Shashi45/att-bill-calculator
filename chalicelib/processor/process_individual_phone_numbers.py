
import re

class process_individual_phone_numbers:

    __wireless_list=[]
    __raw_text=''
    __regex_phone_number_base = '[A-Z]+\s{1}[A-Z]+'

    def __init__(self, raw_text, wireless_list):

        self.__wireless_list = wireless_list
        self.__raw_text = raw_text

    def get_name_from_phone_number(self, phone_number):

        base_regex = r''+phone_number+ self.__regex_phone_number_base
        # print base_regex
        match = re.search(base_regex, self.__raw_text)
        if match:
           # print self.__raw_text[match.start():match.end()-1]
           return self.__raw_text[match.start():match.end()-1]
        else:
           return None

    def get_individual_phone_data(self):
        list_phone_data = list()
        phone_number_data_dict = {}
        for wless in self.__wireless_list:
            name = self.get_name_from_phone_number(wless.get__phone_number())
            wless.set__name(name)
            regex_to_find_phone_data_start = r''+wless.get__name()
            # print regex_to_find_phone_data_start
            regex_to_find_phone_data_end = r''+'Total for'+wless.get__phone_number()+'\d*\.\d{2}'
            # print regex_to_find_phone_data_end
            match_start = re.search(regex_to_find_phone_data_start, self.__raw_text)
            match_end = re.search(regex_to_find_phone_data_end, self.__raw_text)
            if match_start and match_end:
               # print self.__raw_text[match_start.start() : match_end.end()]
               phone_number_data_dict[wless.get__name()] = self.__raw_text[match_start.start() : match_end.end()]
               list_phone_data.append(self.__raw_text[match_start.start() : match_end.end()])
        return phone_number_data_dict
