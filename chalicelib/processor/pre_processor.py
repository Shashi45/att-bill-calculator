
from chalicelib.common.bill_category_enum import bill_category_enum

class pre_processor:

    __bill_cycle_date=''
    __bill_at_glance_raw_data=''
    __service_summary_raw_data=''

    def __init__(self, raw_pdf_string):
        # print "PreProcessor..."
        self.raw_pdf_string = raw_pdf_string;


    def extract_wireless_statement_categroy_raw_txt(self):
        # print "printing... "
        self.extract_bill_at_glance_data();
        self.extract_service_summary_data();
        self.extract_bill_cycle_date();

        find_index = self.raw_pdf_string.find(bill_category_enum.SERVICE_SUMMARY_END.value)
        self.raw_pdf_string = self.raw_pdf_string[find_index:]
        # print "--------->>>>>>>>", self.raw_pdf_string[find_index:]

    def extract_bill_cycle_date(self):
        self.__bill_cycle_date = self.__extract_data(bill_category_enum.BILL_CYCLE_DATE, bill_category_enum.BILL_CYCLE_DATE_END, self.raw_pdf_string)

    def extract_bill_at_glance_data(self):
        # print "extracting bill at glance data"
        self.__bill_at_glance_raw_data = self.__extract_data(bill_category_enum.BILL_AT_GLANCE, bill_category_enum.BILL_AT_GLANCE_END, self.raw_pdf_string)


    def extract_service_summary_data(self):
        self.__service_summary_raw_data = self.__extract_data(bill_category_enum.SERVICE_SUMMARY, bill_category_enum.SERVICE_SUMMARY_END, self.raw_pdf_string)
        # print "sub_string", sub_string
        # print "---",self.__service_summary_raw_data

    def __extract_data(self, start_text_enum, end_text_enum, raw_text):
        # print "extracting... ", start_text_enum.value
        start_index = raw_text.rfind(start_text_enum.value)
        end_index = raw_text.find(end_text_enum.value)
        # print "start_index ", start_index, " end_index ", end_index
        sub_string = raw_text[start_index:end_index]
        # print "sub_string ", sub_string
        return sub_string


    def get__bill_at_glance_raw_data(self):
        return self.__bill_at_glance_raw_data

    def get__service_summary_raw_data(self):
        return self.__service_summary_raw_data
