

import json
from chalicelib.modal.response.att_response  import att_response
from chalicelib.modal.response.individual_bill_response import individual_bill_response

class bill_calculator:

    __account_details = ''
    __service_summary_obj = ''
    __common_specific_items_dict = {}
    __common_items = []
    __specific_items = []
    should_show_detail_charges = ''

    def get__common_items(self):
        return self.__common_items
    def set__common_items(self, item):
        self.__common_items = item

    def __init__(self, account_details, should_show_detail_charges):
        # print
        self.__account_details = account_details
        self.should_show_detail_charges = should_show_detail_charges
        self.__common_items = []
        self.__specific_items = []

        self.__common_specific_items_dict['Access for '] = 'common'
        self.__common_specific_items_dict['Discount for Access'] = 'common'
        self.__common_specific_items_dict['Administrative Fee'] = 'common'
        self.__common_specific_items_dict['Federal Universal Service Charge'] = 'common'
        self.__common_specific_items_dict['Regulatory Cost Recovery Charge'] = 'common'
        self.__common_specific_items_dict['State Access Fund'] = 'common'
        self.__common_specific_items_dict['State Telecom Surcharge'] = 'common'
        self.__common_specific_items_dict['State Sales Surcharge'] = 'common'
        self.__common_specific_items_dict['Mobile Share '] = 'common'
        self.__common_specific_items_dict['Multi-Device Protection Pack - Insurance'] = 'common'
        self.__common_specific_items_dict['Multi-Device Protection Pack - Support'] = 'common'
        self.__common_specific_items_dict['National Account Discount'] = 'common'
        self.__common_specific_items_dict['State Cost Recovery Fee'] = 'common'
        # State Access Fund
        # State Telecom Surcharge
        # Regulatory Cost Recovery Charge

        # Mobile Share Value
        # Multi-Device Protection Pack - Insurance
        # National Account Discount

        # 9-1-1 Service Fee
        # County 9-1-1 Service Fee

        self.__common_specific_items_dict['County 9-1-1 Service Fee'] = 'common'
        self.__common_specific_items_dict['9-1-1 Service Fee'] = 'common'
        self.__common_specific_items_dict['See News You Can Use'] = 'common'
        self.__common_specific_items_dict['Messages Backup'] = 'common'
        self.__common_specific_items_dict['data used'] = 'common'

        self.__common_specific_items_dict['County District Sales Tax'] = 'common'
        self.__common_specific_items_dict['County Sales Tax'] = 'common'
        self.__common_specific_items_dict['State Sales Tax'] = 'common'
        self.__common_specific_items_dict['State/Local Wireless'] = 'common'
        self.__common_specific_items_dict['Messages'] = 'common'
        self.__common_specific_items_dict['International Long Distance'] = 'specific'
        self.__common_specific_items_dict['Installment'] = 'specific'
        self.__common_specific_items_dict['Minutes Billed'] = 'specific'
        self.__common_specific_items_dict['Upgrade Fee'] = 'specific'
        self.__common_specific_items_dict['Answer Tone: Text Collect'] = 'specific'
        # self.__common_specific_items_dict['Access for iPhone 4G LTE w/ Visual Voicemail'] = 'specific'
        # self.__common_specific_items_dict['Access for iPhone 4G LTE'] = 'specific'
        # self.__common_specific_items_dict['Discount for Access'] = 'specific'
        # self.__common_specific_items_dict['Installment'] = 'specific'
        self.segrate_common_and_specific()


    def segrate_common_and_specific(self):
        service_summary = self.__account_details.get__service_summary()
        wless = service_summary.get__wirelsss_list()

        for wl in wless:
            # print json.dumps(wl, default=lambda o: o.__dict__, sort_keys=True, indent=2)
            self.__specific_items = []
            self.filter_items_list(wl, wl.get__monthly_charges().get__item_list())
            if wl.get__other_credit_and_tax_charges().get__account_activity():

                self.filter_items_list(wl, wl.get__other_credit_and_tax_charges().get__account_activity().get__items())
            if wl.get__other_credit_and_tax_charges().get__data_usage():
                # print "-------------------------------",wl.get__other_credit_and_tax_charges().get__data_usage()
                self.check_item_type(wl, wl.get__other_credit_and_tax_charges().get__data_usage().get__item(), self.__common_items, self.__specific_items)
            if wl.get__other_credit_and_tax_charges().get__voice_usage_summary():
                self.check_item_type(wl, wl.get__other_credit_and_tax_charges().get__voice_usage_summary().get__item(), self.__common_items, self.__specific_items)
            if wl.get__other_credit_and_tax_charges().get__wireless_equip_charges():
               self.check_item_type(wl, wl.get__other_credit_and_tax_charges().get__wireless_equip_charges().get__item(), self.__common_items, self.__specific_items)
            if wl.get__other_credit_and_tax_charges().get__one_time_charges():
               # print "----------", wl.get__other_credit_and_tax_charges().get__one_time_charges().get__item()
               self.check_item_type(wl, wl.get__other_credit_and_tax_charges().get__one_time_charges().get__item(), self.__common_items, self.__specific_items)
            if wl.get__other_credit_and_tax_charges().get__other_purchases_3rd_party():
               # print "----------", wl.get__other_credit_and_tax_charges().get__other_purchases_3rd_party().get__items()[0]
               self.filter_items_list(wl, wl.get__other_credit_and_tax_charges().get__other_purchases_3rd_party().get__items())
            self.filter_items_list(wl, wl.get__other_credit_and_tax_charges().get__surcharges().get__items())
            self.filter_items_list(wl, wl.get__other_credit_and_tax_charges().get__govt_fees_and_taxes().get__items())
            wl.set__specific_items(self.__specific_items)
            # print wl

            # print "******specific*******", self.__common_items
        service_summary.set__common_items_list(self.__common_items)
        # print "******common*******"
        # for itm in self.__common_items:
        #     print itm
        # print "******specific*******"
        # for itm in self.__specific_items:
        #     print itm
        # print "------serviceSummary--------------",service_summary.get__common_items_list()


    def filter_items_list(self, wl, items_list):

        for it in items_list:
            it.set__wireless_number(wl.get__phone_number())
            self.check_item_type(wl, it, self.__common_items, self.__specific_items)

    def check_item_type(self, wl, it, common, specific):

        for key in self.__common_specific_items_dict.keys():
            # print "((((((((((()))))))))))", it.get__type()
            # print "(((((((((((", key
            # print ")))))))))))",(key in it.get__type())
            if key in it.get__type():
               if self.__common_specific_items_dict[key] == 'common':
                  # wl.set__common_items(it)
                  common.append(it)
                  break
                # return it
               elif self.__common_specific_items_dict[key] == 'specific':
                    # print "^^^^^",it.get__type()
                    # wl.set__specific_items(it)
                    specific.append(it)
                    # return it
        # print "-----",self.__common_items


    def calculate(self):
        # print "calculate ", self.__service_summary_obj
        # res = att_response()
        service_summary = self.__account_details.get__service_summary()
        wless = service_summary.get__wirelsss_list()
        common_total = self.get_total_common_items_price()
        ind_commont_total = common_total/len(wless)
        # print "bill cycleeeeeeeeeeeee", self.__account_details.get__bill_cycle_date()
        # print "common toatllllllllllllllllll", common_total
        lst = list()
        actual_total = 0
        for w in wless:
            # print "wireless number", w.get__phone_number()
            res = individual_bill_response()
            total = 0
            for it in w.get__specific_items():
                if it.get__is_item_credit() == 'yes':
                   total -= float(it.get__price())
                else:
                    total += float(it.get__price())
            # print "spec is ", total
            res.setspecific_charges(total)

            total = ind_commont_total + total
            res.settotal_charges(total)
            # res.setspecific_charges(total)
            actual_total += total

            res.setcommon_charges(ind_commont_total)
            # res.setbill_cycle(self.__account_details.get__bill_cycle_date())
            res.setwireless_number(w.get__phone_number())
            if self.should_show_detail_charges:
               res.setspecific_charges_list(w.get__specific_items())
            lst.append(res)
            # print "final Individual total%%%%%%%%%%%%%%", total
        # print "account total balance ", self.__account_details.get__total_balance()
        # print "expected total ", self.__account_details.get__current_balance()
        # print "actual total ",(actual_total + 5.02)
        # print "------------------BILL End------------------"
        # res.setwireless_number('123')

        # lst.append(res)
        print "000000--------total balance-------0000000000", self.__account_details.get__current_balance()
        att_response_obj = att_response()
        att_response_obj.settotal_charges(self.__account_details.get__current_balance())
        att_response_obj.setbill_cycle(self.__account_details.get__bill_cycle_date())
        if self.should_show_detail_charges:
           att_response_obj.setcommon_charges_list(self.__account_details.get__service_summary().get__common_items_list())
        att_response_obj.setindividual_bill_details(lst)
        return att_response_obj



    def get_total_common_items_price(self):
        service_summary = self.__account_details.get__service_summary()
        total = 0.00
        for it in service_summary.get__common_items_list():
            # print "it--->",it
            if it.get__is_item_credit() == 'yes':
               total -= float(it.get__price())
            else:
                total += float(it.get__price())
        return total
